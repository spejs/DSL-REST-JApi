#DSL REST JApi 

A simple DSL for generating classes for Java REST API using .jg Files.
Work in progress.. Still got to implement @Produces and @Consumes

Things todo:
Add documentation
Add produces/consume annotations 

t.jg File
```javascript 
class  TemperatureCelsius{
	GET  String getCurrentTemperature 
	params{
		float longtitude
		float latitude
	}
	path "weather/temperature/celsius"   
	doc{ 
		@author "Mikolaj"
		@version "0.0.1"    
		"To create a multi-line comment"
		"Just write your comment in multiple strings"
		"Just like this."
	}

	
	GET String getCurrendTemperatureForRandomLocation
	
	POST void updateCurrentWeather 
	params{
		float longtitude
		float latitude
		int temperatureCelsius
	}
}

```
Generated TemperatureCelsius.java
```javascript
package rest.japi;

import javax.ws.rs.*


@Path("/TemperatureCelsius")
public class TemperatureCelsius {
	
	/**
	*@author Mikolaj 
	*@version 0.0.1 
	*To create a multi-line comment
	*Just write your comment in multiple strings
	*Just like this.
	*/
	@Path("/weather/temperature/celsius")	
	@GET
	public String getCurrentTemperature(float longtitude, float latitude ) throws JSONException{
		//TODO: implement
	}
			
	/**
	*/
	@GET
	public String getCurrendTemperatureForRandomLocation() throws JSONException{
		//TODO: implement
	}
			
	/**
	*/
	@POST
	public void updateCurrentWeather(float longtitude, float latitude, int temperatureCelsius ) throws JSONException{
		//TODO: implement
	}
			
}
```
Generated TemperatureFahrenheit.java
```javascript
package rest.japi;

import javax.ws.rs.*


@Path("/TemperatureFahrenheit")
public class TemperatureFahrenheit {
	
	/**
	*/
	@GET
	public String getCurrentTemperature(float longtitude, float latitude ) throws JSONException{
		//TODO: implement
	}
			
	/**
	*/
	@GET
	public String getCurrendTemperatureForRandomLocation() throws JSONException{
		//TODO: implement
	}
			
	/**
	*/
	@POST
	public void updateCurrentWeather(float longtitude, float latitude, int temperatureFahrenheit ) throws JSONException{
		//TODO: implement
	}
			
}

```
