/*
 * generated by Xtext 2.9.1
 */
package spejss.com.github.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import spejss.com.github.japiGeneratorDSL.RestClass
import spejss.com.github.japiGeneratorDSL.AttributeType
import spejss.com.github.japiGeneratorDSL.MethodType

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class JapiGeneratorDSLGenerator extends AbstractGenerator {

	/**
	 * Iterate trough the list of RestClasses in the Model and generate files.
	 * Fill them with content specified in the compile function
	 */
	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		for(rc : resource.allContents.toIterable.filter(typeof(RestClass))){
			fsa.generateFile(
				"rest/" + rc.name + ".java", rc.compile
			)
		}
	}
	
	def compile(RestClass rc){
		'''
		package rest.japi;
		
		import javax.ws.rs.*
		
		
		@Path("/«rc.name»")
		public class «rc.name» {
			«FOR attribute : rc.attributes»
				private «attribute.type.compile» «attribute.name»;
			«ENDFOR»
			«FOR attribute : rc.attributes»
				public «attribute.type.compile» get«attribute.name.toFirstUpper»() {
					return «attribute.name»;
				}
			
				public void set«attribute.name.toFirstUpper»(«attribute.type.compile» _arg) {
					this.«attribute.name» = _arg;
				}
			«ENDFOR»
			
		«FOR function : rc.functions» 
				«IF function.annotations != null || function.comments != null»
					/**
					«FOR annotation : function.annotations»
						*@«annotation.name» «annotation.value» 
					«ENDFOR»
					«FOR comment : function.comments»
						*«comment.value»
					«ENDFOR»
					*/
				«ENDIF»
					«IF function.paths != null»
						@Path("/«function.paths.value»")	
					«ENDIF»
					«IF function.prodTypes != null»
						@Produces("application/«function.prodTypes.name»")	
					«ENDIF»
					«IF function.consTypes != null»
						@Consumes("application/«function.consTypes.name»")	
					«ENDIF»
					@«function.method.compile»
					public «function.returnValue.compile» «function.name»(«FOR param : function.parameters SEPARATOR ','»«param.type.compile» «param.name» «ENDFOR») throws JSONException{
						//TODO: implement
					}
			
		«ENDFOR»
			}
		'''
	}
	
	 	def compile(AttributeType attributeType) {
		attributeType.typeToString +
			if (attributeType.array) "[]"
			else ""
	}
	
	def compile(MethodType attributeType) {
		attributeType.typeToString
	}
	
	def dispatch typeToString(MethodType type) {
		if (type.typeName == "string") "String"
		else type.typeName
	}
	
	def dispatch typeToString(AttributeType type) {
		if (type.typeName == "string") "String"
		else type.typeName
	}
	
}
